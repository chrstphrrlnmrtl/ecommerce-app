import {Card, Button, CardGroup} from 'react-bootstrap'
import {Link} from 'react-router-dom';

import brandX from '../images/brandx.webp'


export default function ProductCard({productProp}){
    const {_id, productName, price, stock} = productProp;

    return(
      <CardGroup className = 'mb-5'>
          <Card  className = "cardProducts mt-5 mb-5 text-center" id = 'prod-cards'>
            <Card.Img variant="top" src={brandX}  width = '200px' height='300px'/>
            <Card.Body>
              <Card.Title><strong>{productName}</strong></Card.Title>
              <Card.Text>P {price}.00</Card.Text>
              <Card.Text>There are {stock} stocks as of the moment.</Card.Text>
            </Card.Body>
            <Card.Footer>
              <Button as = {Link} to={`/products/${_id}`}variant="primary" className = "mx-10">
                Details
              </Button>
            </Card.Footer>
          </Card>
     </CardGroup>
        
    
    )
}