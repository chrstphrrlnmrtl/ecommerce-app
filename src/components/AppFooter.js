
export default function AppFooter(){
   
    return(
        <div id="footer" class="text-center text-light p-3">
            <p class="m-0">Christopher O. Mortel</p>
            <p>Copyright 2022</p>
            <p>All rights reserved. Powered by the <a href="https://github.com/" target="_blank" class="text-light">Github</a>.</p>
        </div>
    )
}