import React from "react";
import {Routes, Route, useLocation} from "react-router-dom";
import Home from '../pages/Home'
import Register from '../pages/Register'
import Login from '../pages/Login';
import Products from '../pages/Products';
import AdminDashboard from '../pages/AdminDashboard';
import Logout from '../pages/Logout';
import ProductView from '../pages/ProductView';
import PageNotFound from '../pages/Page404';
import Orders from '../pages/Orders';
import UserOrder from '../pages/UserOrder';


import {AnimatePresence} from "framer-motion";



export default function AnimatedRoutes(){
    const location = useLocation()
    return(
    <AnimatePresence>
     <Routes location = {location} key = {location.pathname}>
            <Route exact path = "/" element = {<Home/>}/>
            <Route exact path = "/register" element = {<Register/>}/>
            <Route exact path = "/login" element = {<Login/>}/>
            <Route exact path = "/logout" element = {<Logout/>}/>
            <Route exact path = "/products" element = {<Products/>}/>
            <Route exact path = "/products/:productId" element = {<ProductView/>}/>
            <Route exact path = "/allproducts" element ={<AdminDashboard/>}/>
            <Route exact path = "*" element ={<PageNotFound/>}/>
            <Route exact path = "/orders" element ={<Orders/>}/>
            <Route exact path = "/userorders" element ={<UserOrder/>}/>
        </Routes>
    </AnimatePresence>

    )
}