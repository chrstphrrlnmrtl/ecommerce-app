import { useEffect, useState } from 'react';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import AppNavBar from "./components/AppNavBar";
import {Container} from "react-bootstrap"
import {UserProvider} from "./UserContext";
import './App.css';
import AnimatedRoutes from './components/AnimatedRoute';





 function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    name: null,
    lastName: null
  });

  const unsetUser = () => {localStorage.clear();}

  useEffect(()=>{}, [user]);

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}users/details`,{
      headers: {
        Authorization : `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(response => response.json())
    .then(data => {
     
      if(data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          name: data.firstName,
          lastName: data.lastName
        })
      }
      else{
        setUser({
          id: null,
          isAdmin: null,
          name: null,
          lastName: null

        })
      }
    })
  }, [])

 
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavBar/>
        <Container>
            <AnimatedRoutes/>
        </Container>
     
        
    </Router>
    </UserProvider>
   
  );
}

export default App;
