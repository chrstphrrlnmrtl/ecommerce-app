import ProductCard from "../components/ProductCard"
import {useContext, useEffect, useState} from 'react';
import {Row} from "react-bootstrap";
import UserContext from "../UserContext";
import { Navigate, NavLink} from 'react-router-dom';

import {motion} from "framer-motion";

export default function Products(){
    const [products, setProducts] = useState([]);
    const {user} = useContext(UserContext);

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}products/activeproducts`)
        .then(response => response.json())
        .then(data => {
           
            setProducts(data.map(product=>{
                return (
                    <ProductCard key={product._id} productProp={product}/>
                )
            }))
        })
    },[])
   
    return(
        (user.isAdmin)
        ?
        <Navigate as = {NavLink} to = "/"/>
        :
        <motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}}>
        <>
             <h1 className="text-center m-3"><strong>Products List</strong></h1>
        <Row md={3} className="g-3 mb-3" >
                 {products}
        </Row>
        
        </>
        </motion.div>
    )
}