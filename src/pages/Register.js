import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useNavigate, Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import {motion} from "framer-motion";

export default function Register(){

  const [fName, setFName] = useState('');
  const [lName, setLName] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] =useState('');
  const [age, setAge] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [address, setAddress] =useState('');

  const {user} = useContext(UserContext);
  const navigate = useNavigate();

  const [isActive, setIsActive] = useState(false);

  useEffect(() => {

    if(fName !== '' && lName !== '' && password1 !=='' && email!=='' && password2 !=='' && age !== '' && mobileNumber !== '' && address !=='' && password1 === password2){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }

  }, [fName, lName, email, password1, password2, age, mobileNumber, address])

      function registerUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}users/checkemail`, {
          method: "POST",
          headers: {
            "Content-Type" : "application/json"
          },
          body: JSON.stringify({
            email: email
          })
        })
        .then(response => response.json())
        .then(data => {
          if(data){
            Swal.fire({
              title: "Duplicate email found",
              icon: "error",
              text: "Kindly provide another email to complete the registration."
            })
          }
          else{
            fetch(`${process.env.REACT_APP_API_URL}users/checkage`, {
              method: "POST",
              headers: {
                "Content-Type" : "application/json"
              },
              body: JSON.stringify({
                age: age
              })
            })
            .then(response => response.json())
            .then (data => {
              if(data){
                
                Swal.fire({
                  title: "Underage!",
                  icon: "error",
                  text: "You need to be atleast 18 years old to register."
                })
              }
              else{
                fetch(`${process.env.REACT_APP_API_URL}users/register`, {
                  method: "POST",
                  headers:{
                    "Content-Type" : "application/json"
                  },
                  body: JSON.stringify({
                    firstName: fName,
                    lastName:lName,
                    email: email,
                    password: password1,
                    age : age,
                    mobileNumber: mobileNumber,
                    deliveryAddress: address
                  })
                })
                .then(response => response.json())
                .then(data =>{
                  if (data){
                    Swal.fire({
                      title: "Registration Successful!",
                      icon: "success",
                      text: "Welcome to PHOEBE's Shop!"
                    })

                      setFName('');
                      setLName('');
                      setEmail('');
                      setAge('');
                      setMobileNumber('');
                      setAddress('');
                      setPassword1('');
                      setPassword2('');


                      navigate("/login")
                  }
                  else{
                    Swal.fire({
                      title: "Something went wrong!",
                      icon: "error",
                      text: "Please try again!"
                    })
                  }
        
                })
              }
            })

          }
        })
        
        
      }
    return(
      (user.id !== null)
		?
			<Navigate to = "/login"/>
		:
    <motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}} id= "reg" className = 'mt-5 mb-5'>
    <h1 className="text-center register text-dark mt-5"> Register</h1>
    <Form onSubmit={e => registerUser(e)} className = 'p-5'>
    <Form.Group className="mb-3" controlId="fName">
        <Form.Label>First Name</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter your First Name"
        value = {fName}
        onChange = {e => setFName(e.target.value)}
        required/>
    </Form.Group>

    <Form.Group className="mb-3" controlId="lName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter your Last Name"
        value={lName}
        onChange = {e => setLName(e.target.value)}
        required/>
    </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        placeholder="Enter email"
        value = {email}
        onChange = {e => setEmail(e.target.value)}
        required/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Enter your password"
        value = {password1}
        onChange = {e => setPassword1(e.target.value)}
        required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Confirm  your password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Verify your password" 
        value = {password2}
        onChange = {e => setPassword2(e.target.value)}
        required/>
      </Form.Group>

    <Form.Group className="mb-3" controlId="age">
        <Form.Label>Age</Form.Label>
        <Form.Control 
        type="number" 
        placeholder="Enter your age"
        value = {age}
        onChange = {e => setAge(e.target.value)}
        required />
    </Form.Group>

    <Form.Group className="mb-3" controlId="mobileNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control 
        type="number" 
        placeholder="Enter your Mobile Number" 
        value = {mobileNumber}
        onChange = {e => setMobileNumber(e.target.value)}
        required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="deliveryAddress">
        <Form.Label>Delivery Address</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter your Mobile Number" 
        value = {address}
        onChange = {e => setAddress(e.target.value)}
        required/>
      </Form.Group>
    {
      isActive ? <Button variant="primary" type="submit" id = "submitBtn">
                     Register
                  </Button>
      :
      <Button variant="danger" type="submit" id = "submitBtn" disabled>
        Register
      </Button>
    }
      
    </Form>
    </motion.div>
    )
}