import {Button, Form, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate, NavLink} from 'react-router-dom';

import logo from '../images/logo.png';
import {motion} from "framer-motion";


export default function Login(){
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const {user, setUser} = useContext(UserContext);
  const [isActive, setIsActive] = useState(false);

  useEffect(() =>{
      if(email !== '' && password !== ''){
        setIsActive(true)
      }
      else{
        setIsActive(false)
      }
  }, [email, password])


  function loginUser(e){
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}users/login`, 
    {
      method: "POST",
      headers:{
        "Content-Type" : "Application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => response.json())
    .then(data => 
      {
      if(data.accessToken !== undefined){
        localStorage.setItem("token", data.accessToken)

        retrieveUserDetails(data.accessToken)
        
        Swal.fire({
          title: "Login Successful!",
          icon: "success",
          text: "Welcome to Phoebe's!" 
        })
        setEmail('');
        setPassword('');
      }
      else{
        Swal.fire({
          title: "Authentication failed!",
          icon: "error",
          text: "Check your login details and try again!"})
      }
    })

    const retrieveUserDetails = (token) => {
      fetch(`${process.env.REACT_APP_API_URL}users/details`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(response => response.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          name: data.firstName,
          lastName: data.lastName 
        })
      })
    }

  }

    return(

      (user.id !== null)
      ?
      
        (user.isAdmin)
        ?
        <Navigate as = {NavLink} to = "/allproducts"/>
         :
        <Navigate as = {NavLink} to = "/products"/>
      
       :
<motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}}>    
<Row >
       <Col  id = 'login' className = "mb-5 mt-5 ">
       <h1 className="text-center mt-5">Login</h1>
       <Form onSubmit = {e => loginUser(e)}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control 
          type="email" 
          placeholder="Enter email" 
          value = {email}
          onChange = {e => setEmail(e.target.value)}
          required
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
  
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control 
          type="password" 
          placeholder="Password" 
          value ={password}
          onChange = {e => setPassword(e.target.value)}
          />
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Text className="text-muted">
            Don't have account? <a href = "/register">Sign up.</a>
          </Form.Text>
        </Form.Group>
        {
          isActive ? <Button variant="primary" type="submit" id = "loginBtn" className="mb-5">
                           Login
                    </Button>
          :
                    <Button variant="danger" type="submit" id ="loginBtn" className = "mb-5"disabled>
                      Login
                    </Button>
        }
           
      </Form>
      
       </Col>
       <Col>
        <img 
          className="d-block h-100 mt-5 mx-5"
          src={logo}
          alt="First slide"

         />
       </Col>
       </Row>
       </motion.div>
      
    )
}