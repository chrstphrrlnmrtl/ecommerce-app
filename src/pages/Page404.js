import {Row, Col, Button} from "react-bootstrap"
import { Link } from "react-router-dom";

import {motion} from "framer-motion";

export default function PageNotFound(){
    return(
		<motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}}>
        <Row>
			<Col className = "p-5 text-center">
				<h1>Error 404, page not found</h1>
				<p>It seems that you are lost.</p>
				<Button as = {Link} to ="" variant ="primary" >I'll lead you to the home.</Button>
			</Col>
		</Row>
		</motion.div>
    )
}