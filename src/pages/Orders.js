import {Table} from 'react-bootstrap';
import {useState} from 'react';


import {motion} from "framer-motion";

export default function Orders(){

    const [allOrders, setAllOrders] = useState([])
    
    fetch(`${process.env.REACT_APP_API_URL}users/allorders`,{
        headers: {
            "Authorization" : `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(response => response.json())
    .then(data => {
       
        setAllOrders(data.map(order => {
                return(
                    <tr key = {order._id}>
                        <td>{order._id}</td>
                        <td>{order.product[0].productName}</td>
                        <td>{order.product[0].quantity}</td>
                        <td>{order.totalAmount}</td>
                        <td>{order.dateAdded}</td>
                    </tr>)
            }))})
            

   
    return(
      <motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}}>
       <Table striped>
      <thead>
        <tr>
          <th>Transaction Code</th>
          <th>Product Name</th>
          <th>Quantity</th>
          <th>Total Amount</th>
          <th>Date Ordered</th>
        </tr>
      </thead>
      <tbody>
        {allOrders}
      </tbody>
    </Table>
    </motion.div>
    )
}