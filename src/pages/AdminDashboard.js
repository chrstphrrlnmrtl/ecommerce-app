import {useEffect, useState, useContext} from 'react';
import {Modal, Form, Button, Table} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

import {motion} from "framer-motion";

import UserContext from '../UserContext';

export default function Products(){
    const{ user } = useContext(UserContext);

    const [allProducts, setAllProducts] = useState([]);

    const [productId, setProductId] = useState("");
    const [productName, setProductName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStocks] = useState(0);

    const [isActive, setIsActive] = useState(false);

    const [showAdd, setShowAdd] = useState(false);
    const [showEdit, setShowEdit] = useState(false);

    const openAdd = ()=> setShowAdd(true);
    const closeAdd = () => setShowAdd(false);

    const openEdit = (id) => {
        setProductId(id);

        fetch(`${process.env.REACT_APP_API_URL}products/${id}`)
        .then(response => response.json())
        .then(data => {
            setProductName(data.productName);
            setDescription(data.description);
            setPrice(data.price);
            setStocks(data.stock);

            setShowEdit(true);
        })
    }

    const closeEdit = () => {
        setProductName('');
        setDescription('');
        setPrice('');
        setStocks('');
            setShowEdit(false);
    }

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}products/allProducts`, {
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            setAllProducts(data.map(product=>{
                
                return(
                    <tr key = {product._id}>
                        <td>{product._id}</td>
                        <td>{product.productName}</td>
                        <td>{product.description}</td>
                        <td>{product.price}</td>
                        <td>{product.stock}</td>
                        <td>{product.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            {
                                (product.isActive)
                                ?
                                <Button variant="danger" size="sm" onClick={() => archive(product._id, product.productName)}>Archive</Button>
                                :
                                <>
                                    <Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.productName)}>Unarchive</Button>
								    <Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
                                </>
                                

                            }
                        </td>
                    </tr>
                )
            }))
        })
    }

    useEffect(()=> {
        fetchData();
    }, [])

    const archive = (id, productName) => {
        fetch(`${process.env.REACT_APP_API_URL}products/${id}`,{
            method: "PATCH",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(response => response.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
                    
                })
                fetchData();
            }
            else{
                Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});

            }
        })
    }

    const unarchive = (id, productName) => {
        fetch(`${process.env.REACT_APP_API_URL}products/${id}`,{
            method: "PATCH",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(response => response.json())
        .then(data => {
            if(data){
                Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
                    fetchData();
            }
            else{
                Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});

            }
        })
    }

    const addProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}products/`, {
            method : "POST",
            headers : {
                "Content-Type" : "Application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productName: productName,
                description: description,
                price: price,
                stock: stock
            })
        })
        .then(response => response.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Product succesfully Added",
                    icon: "success",
                    text: `${productName} is now added`
                });
                fetchData();

                closeAdd();
            }
            else if(data === false ){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `The Product is already listed. Add another products.`
                });
                closeAdd();
            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `Something went wrong. Please try again later!`
                });
                closeAdd();
            }
        })

        setProductName('');
        setDescription('');
        setPrice('');
        setStocks('');
    }

    const editProduct = (e) => {
        e.preventDefault();
        
        fetch(`${process.env.REACT_APP_API_URL}products/update/${productId}`, {
            method : "PUT",
            headers: {
                "Content-Type" : "Application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productName: productName,
                description: description,
                price: price,
                stock: stock
            })
        })
        .then(response => response.json())
        .then(data =>{
            if(data){
                Swal.fire({
                    title: "Product succesfully Updated",
                    icon: "success",
                    text: `${productName} is now updated`
                });
            
                fetchData();
                closeEdit();

            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `Something went wrong. Please try again later!`
                });

                closeEdit();
            }
        })
        setProductName('');
        setDescription('');
        setPrice(0);
        setStocks(0);

    }

    useEffect(()=> {
        if(productName !== '' && description !== '' && price !== '' && stock > 0){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [productName, description, price, stock])


    
   
    return(
        
        (user.isAdmin)
        ?
        <motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}}>
        <>
            <div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Product</Button>
				{/*To view all the user enrollments*/}
				{/* <Button variant="secondary" className="
				mx-2">Show All User Orders</Button> */}
			</div>
              <Table striped bordered hover>
      <thead>
        <tr>
          <th>Product Id</th>
          <th>Product Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Stocks</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
            {allProducts}
      </tbody>
    </Table>

    <Modal show={showAdd}  onHide={closeAdd}>
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="productName" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {productName}
	    		                onChange={e => setProductName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Product Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="stocks" className="mb-3">
	    	                <Form.Label>Product Stocks</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Stocks" 
	    		                value = {stock}
	    		                onChange={e => setStocks(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>


            <Modal show={showEdit}  onHide={closeEdit}>
    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Product Name" 
    		                value = {productName}
    		                onChange={e => setProductName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Product Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Product Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="slots" className="mb-3">
    	                <Form.Label>Product Stocks</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Slots" 
    		                value = {stock}
    		                onChange={e => setStocks(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>


        </>
        </motion.div>
    :
    <Navigate to = "/products"/>
    
    )
}